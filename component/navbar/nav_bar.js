import { Navbar, Nav, NavDropdown, Container } from "react-bootstrap";
import classes from "./nav_bar.module.css";
import { useRouter } from "next/router";

export default function NavBar() {
  const router = useRouter();

  function returnHome(){
    router.replace("/")
  }

  function jumpToSeating_plan() {
    router.replace("/seating_plan");
  }
  function jumpToPreWeddingPhotos() {
    router.replace("/pre-wedding_photos");
  }
  function jumpToBigDayPhotos() {
    router.replace("/bigday_photos");
  }
  function jumpToBigDayVideos() {
    router.replace("/bigday_videos");
  }

  return (
    <Navbar expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand onClick={returnHome}>{"Helen & Nicholas"}</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link onClick={jumpToSeating_plan}>Seating Plan</Nav.Link>
            <Nav.Link onClick={jumpToPreWeddingPhotos}>Pre-Wedding Photos</Nav.Link>
            <Nav.Link onClick={jumpToBigDayPhotos}>Big Day Photos</Nav.Link>
            <Nav.Link onClick={jumpToBigDayVideos}>Big Day Videos</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
