import { useRouter } from 'next/router'

export default function Demo(){
    const router = useRouter()

    function jumpToHello(){
        router.replace('/hello')
    }


    return(
        <div>
            <div onClick={jumpToHello}>Jump to hello page</div>
            <div>Jump to seating_plan page</div>
        </div>
    )
}