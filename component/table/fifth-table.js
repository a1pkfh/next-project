import classes from "./fifth-table.module.css"

export default function TableContent(props) {
    const names = props.name

    return (
        <div className={classes.table}>
            <div className={classes.topContainer}>
                <div className={classes.seat}>
                    <div className={classes.name1}>{names[0]}</div>
                </div>
                <div className={classes.seat}>
                    <div className={classes.name4}>{names[1]}</div>
                </div>
            </div>
            <div className={classes.midContainer}>
                <div className={classes.midLeftContainer}>
                    <div className={classes.seat}>
                        <div className={classes.name5}>{names[2]}</div>
                    </div>
                </div>
                <div className={classes.mainTable}>
                    <div className={classes.main}>Main Table</div>
                </div>
                <div className={classes.midRightContainer}>
                    <div className={classes.seat}>
                        <div className={classes.name7}>{names[3]}</div>
                    </div>
                </div>
            </div>
            <div className={classes.bottomContainer}>
                <div className={classes.seat}>
                    <div className={classes.name9}>{names[4]}</div>
                </div>
                <div className={classes.seat}>
                    <div className={classes.name12}>{names[5]}</div>
                </div>
            </div>
        </div>
    )
}