import Image from 'next/image'
import classes from "./homeContent.module.css"
import logo from "../../public/logo.png"

export default function HomeContent() {
    return (
        <div className={classes.supercontainer}>
            <div className={classes.container1}>
                <div className={classes.air}></div>
                <Image src={logo} alt="wedding logo" />
                <div className={classes.word}>Wedding Day</div>
                
            </div>
            <div className={classes.container2}>
                <div className={classes.date}>3 OCTOBER 2021</div>
                <div className={classes.time}>CEREMONY AT 11:00am</div>
                <div className={classes.time}>BANQUET AT 1:00pm</div>
                <div className={classes.location}>POOLHOUSE, 11/F,</div>
                <div className={classes.location}>GRAND HYATT HONG KONG,</div>
                <div className={classes.location}>1 HARBOUR RD, WAN CHAI</div>
            </div>
        </div>
    )
}