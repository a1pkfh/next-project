import NavBar from "../component/navbar/nav_bar";
import classes from "./seating_plan.module.css";
import TableContent from "../component/table/table";
import { useState } from "react";
import { useRouter } from "next/router";

export default function Seating_PlanContent() {
  const router = useRouter();

  const [openTableTab, setOpenTableTab] = useState(classes.show);
  const [backToPage, setBackToPage] = useState(classes.nothing);
  const [names, setNames] = useState([]);

  const fontFirstTable = (tableName) => {
    setOpenTableTab(classes.nothing);
    setBackToPage(classes.show);
    setNames(tableName);
  };

  const backToPageClick = () => {
    setOpenTableTab(classes.show);
    setBackToPage(classes.nothing);
  };

    function jumpTo11F() {
    router.replace("/seating_plan");
  }

  function jumpTo5F() {
    router.replace("/seating_plan_fifth_floor");
  }

  const table1 = [
    "李敏嘉",
    "新娘",
    "新郎",
    "關子俊",
    "林子軒",
    "二姑媽",
    "沛鋒爸爸",
    "沛鋒媽媽",
    "昌叔叔",
    "民叔叔",
    "沛鋒爺爺",
    "沛鋒嫲嫲",
  ];
  const table2 = [
    "蕭女士",
    "外父",
    "外母",
    "哥哥",
    "姑丈",
    "姑媽",
    "二舅父",
    "二舅母",
    "Eddie",
    "Vicky",
    "Anthony",
    "Ivan",
  ];
  const table3 = [
    "奕晨B",
    "大表",
    "表姐夫",
    "Max",
    "細表",
    "梁獻文",
    "攝影師",
    "林瀚銳",
    "劉利平",
    "李志浩",
    "蘇道哲",
    "鄧啟舜",
  ];
  const table4 = [
    "大舅母",
    "大舅父",
    "大姨媽",
    "二姨媽",
    "大表哥",
    "大表嫂",
    "三姨媽",
    "細表哥",
    "Carmen",
    "Robert",
    "Enah",
    "細表嫂",
  ];
  const table5 = [
    "盧曉瑩",
    "易詠雪",
    "歐懿德",
    "伍穎妍",
    "梁家敏",
    "江永豪",
    "蔡詠而",
    "化妝師",
    "黎嘉欣",
    "馬潔玲",
    "楊國榮",
    "楊夫人",
  ];
  const table6 = [
    "譚蕙蓮",
    "甘玉美",
    "周偉文",
    "簡國華",
    "林桂燊",
    "盧俊業",
    "莫燕媚",
    "吳紫君",
    "Rosa",
    "王根法",
    "阿翠",
    "李志康",
  ];
  const table7 = [
    "皺婉芬",
    "王梓瑜",
    "魏浩楠",
    "Alice",
    "鄭洛詩",
    "何詠琳",
    "Janny",
    "張嘉莉",
    "TBC",
    "巫婥汶",
    "黃浩業",
    "梅永健",
  ];

  return (
    <div className={classes.seating_plan}>
      <NavBar />
      <div className={classes.supercontainer && openTableTab}>
        <div className={classes.container1}>
          <div className={classes.smallcontainer1} onClick={jumpTo11F}>11/F</div>
          <div className={classes.smallcontainer2} onClick={jumpTo5F}>5/F</div>
        </div>
        <div className={classes.stage}>Stage</div>
        <div className={classes.container2}>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(table1)}
          >
            新郎主家席
          </div>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(table2)}
          >
            新娘主家席
          </div>
        </div>
        <div className={classes.container3}>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(table3)}
          >
            Table 3
          </div>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(table4)}
          >
            Table 5
          </div>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(table5)}
          >
            Table 6
          </div>
        </div>

        <div className={classes.container4}>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(table6)}
          >
            Table 7
          </div>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(table7)}
          >
            Table 8
          </div>
        </div>
      </div>

      <div className={classes.tableContent && backToPage}>
        <div
          className={classes.tableContentDiv}
          onClick={() => backToPageClick()}
        ></div>
        <TableContent name={names} />
        <div
          className={classes.tableContentDiv}
          onClick={() => backToPageClick()}
        ></div>
      </div>
    </div>
  );
}
