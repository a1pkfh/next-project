import NavBar from "../component/navbar/nav_bar"
import HomeContent from "../component/homeContent/homeContent"

export default function Home() {
  return (
    <div>
    <NavBar />
    <HomeContent />
    </div>
  )
}
