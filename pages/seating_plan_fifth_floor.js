import NavBar from "../component/navbar/nav_bar";
import classes from "./seating_plan.module.css";
import TableContent from "../component/table/fifth-table";
import { useState } from "react";
import { useRouter } from "next/router";

export default function Seating_PlanContent_fifth() {
  const router = useRouter();

  const [openTableTab, setOpenTableTab] = useState(classes.show);
  const [backToPage, setBackToPage] = useState(classes.nothing);
  const [names, setNames] = useState([]);

  const fontFirstTable = (tableName) => {
    setOpenTableTab(classes.nothing);
    setBackToPage(classes.show);
    setNames(tableName);
  };

  const backToPageClick = () => {
    console.log("back to click");
    setOpenTableTab(classes.show);
    setBackToPage(classes.nothing);
  };

  function jumpTo11F() {
    router.replace("/seating_plan");
  }

  function jumpTo5F() {
    router.replace("/seating_plan_fifth_floor");
  }

  const tableA1 = ["Lavender", "Carol", "Vanessa", "Judy", "Hoi Mei", "Karrie"];

  const tableA2 = ["Sheir", "Cherie", "Herru", "Eva", "Darren", "Korean"];

  const tableB1 = ["姑丈公", "姑婆", "表姑丈", "大仔", "細仔", "表姑姐"];

  const tableB2 = ["康叔公", "Fung", "成叔公", "Bruce", "叔婆"];

  return (
    <div className={classes.seating_plan}>
      <NavBar />
      <div className={classes.supercontainer && openTableTab}>
        <div className={classes.container1}>
          <div className={classes.smallcontainer1} onClick={jumpTo11F}>
            11/F
          </div>
          <div className={classes.smallcontainer2} onClick={jumpTo5F}>
            5/F
          </div>
        </div>
        <div className={classes.stage}>Stage</div>
        <div className={classes.container2}>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(tableA1)}
          >
            Table A1
          </div>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(tableA2)}
          >
            Table A2
          </div>
        </div>

        <div className={classes.container3}>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(tableB1)}
          >
            Table B1
          </div>
          <div
            className={classes.smallcontainer}
            onClick={() => fontFirstTable(tableB2)}
          >
            Table B2
          </div>
        </div>
      </div>

      <div className={classes.tableContent && backToPage}>
        <div
          className={classes.tableContentDiv}
          onClick={() => backToPageClick()}
        ></div>
        <TableContent name={names} />
        <div
          className={classes.tableContentDiv}
          onClick={() => backToPageClick()}
        ></div>
      </div>
    </div>
  );
}
