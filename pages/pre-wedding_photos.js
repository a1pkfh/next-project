import NavBar from "../component/navbar/nav_bar";
import classes from "./pre-wedding_photos.module.css";
import React from "react";
import ReactPlayer from "react-player";

export default function PreWeddingPhotos() {
  return (
    <div>
      <NavBar />
      <div className={classes.everything}>
        <div className={classes.header}>Our Pre-Wedding Photos</div>

        <div className="player-wrapper">
          <ReactPlayer 
          url="https://firebasestorage.googleapis.com/v0/b/unithouse-9ec04.appspot.com/o/Wedding_photo_slideshow.mp4?alt=media&token=e5e23249-ab0d-443c-9954-30f990cd5f73" 
          playing="true"
          controls="true"
          width="100%"
          
          />
        </div>
      </div>
    </div>
  );
}
