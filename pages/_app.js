import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.css";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          href="https://allfont.net/allfont.css?fonts=book-antiqua"
          rel="stylesheet"
          type="text/css"
        />
        <meta name="description" content="Love Story Between Helen & Nick"/>
        <title>{'Helen & Nick'}</title>
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
